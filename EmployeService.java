import javax.persistence.TypedQuery;
@Stateless
@LocalBean
public class EmployeService implements EmployeServiceRemote {

@PersistenceContext(unitName = "imputation-ejb")
EntityManager em;


public int ajouterEmploye(){
Role r=null;
Employe e=new Employe("Mohamed","Ali","Med@esprit.tn",r.ADMINISTRATEUR);
em.persist(e);
return 1;
}


public Employe getEmployeByEmailAndPassword(String login, String password) {

TypedQuery<Employe> query = em.createQuery("select e from Employe e where e.login=:login AND e.password=:password", Employe.class); 

query.setParameter("login", login); 
query.setParameter("password", password); 

Employe employe = null; 
try { employe = query.getSingleResult(); }
catch (Exception e) { System.out.println("Erreur : " + e); }
return employe;
} 
}
