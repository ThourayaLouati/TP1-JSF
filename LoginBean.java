@ManagedBean(name="loginBean") 
@SessionScoped
public class LoginBean implements Serializable {
private static final long serialVersionUID = 1L;

private String login; private String password; private Employe employe; 
private Boolean loggedIn;

@EJB
EmployeService employeService; 

//Getters & Setters

public String doLogin()
{
String navigateTo = "null"; 
employe = employeService.getEmployeByEmailAndPassword(login, password); 
if (employe != null && employe.getRole() == Role.ADMINISTRATEUR) {
navigateTo = "/pages/admin/welcome?faces-redirect=true";
loggedIn = true; 
}
else 
{
FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("Bad Credentials"));}
return navigateTo; 
}
public String doLogout()
{FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
return "/login?faces-redirect=true";}
 //G�n�rer un constructeur sans argument, les Getters et les Setters
}
